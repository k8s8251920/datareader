﻿FROM laszlo/containerruntimeglobal_x64_dotnet_build:23.04_7.0.11 AS build-env
WORKDIR /app

COPY ./DataReader ./

RUN dotnet --info \
&& dotnet restore \
&& dotnet clean \
&& dotnet build --no-restore \
&& dotnet publish --no-restore -c Release -o out

FROM laszlo/containerruntimeglobal_x64_dotnet_runtime:23.04_7.0.11
WORKDIR /app
COPY --from=build-env /app/out .
EXPOSE 15000
ENV ASPNETCORE_URLS=http://+:15000
ENTRYPOINT ["dotnet", "DataReader.dll"]

using System.Text.Json.Serialization;
using Ardalis.ApiEndpoints;
using DataReader.Services;
using Microsoft.AspNetCore.Mvc;

namespace DataReader.Endpoints.Update;

public class UpdateBodyValue
{
    [JsonPropertyName("value")] public string? Value { get; set; }
}

public class UpdateQuery
{
    [FromRoute(Name = "id")] public string? Id { get; set; }
    [FromBody] public UpdateBodyValue? BodyValue { get; set; }
}

public class UpdateEndpoint : EndpointBaseAsync.WithRequest<UpdateQuery>.WithActionResult
{
    private readonly ILogger<UpdateEndpoint> _logger;
    private readonly IStorageConnector<string, string> _connector;

    public UpdateEndpoint(ILogger<UpdateEndpoint> logger, IStorageConnector<string, string> connector)
    {
        _connector = connector;
        _logger = logger;
    }

    [HttpPost("/api/update/{id}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(422)]
    [ProducesResponseType(404)]
    public override async Task<ActionResult> HandleAsync([FromRoute] UpdateQuery request,
        CancellationToken cancellationToken = new())
    {
        if (request.Id is null || request.BodyValue?.Value is null)
            return UnprocessableEntity();

        _logger.LogInformation("try to insert a new key {Key}", request.Id);
        var result = await _connector.UpdateAsync(request.Id, request.BodyValue.Value);

        return (ActionResult) (result ? Ok() : NotFound());
    }
}
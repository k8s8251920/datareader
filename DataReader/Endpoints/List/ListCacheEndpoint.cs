﻿using System.Text.Json;
using Ardalis.ApiEndpoints;
using DataReader.Services;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;

namespace DataReader.Endpoints.List;

public class ListCacheEndpoint : EndpointBaseAsync.WithoutRequest.WithActionResult<ListResult>
{
    private readonly ILogger<ListCacheEndpoint> _logger;
    private readonly IStorageConnector<string, string> _storageConnector;

    public ListCacheEndpoint(ILogger<ListCacheEndpoint> logger, IStorageConnector<string, string> storageConnector)
    {
        _logger = logger;
        _storageConnector = storageConnector;
    }

    [HttpGet("/api/listCache")]
    public override async Task<ActionResult<ListResult>> HandleAsync(
        CancellationToken cancellationToken = new())
    {
        _logger.LogInformation("try to read list of elements in cache");
        var result = await _storageConnector.ListAsync();
        var list = result
            .Select(tuple => new TransportDataSet {Id = tuple.Item1, Value = tuple.Item2})
            .ToArray();
        return new ListResult {Results = list};
    }
}
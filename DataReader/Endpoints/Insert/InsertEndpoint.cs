using System.Text.Json;
using System.Text.Json.Serialization;
using Ardalis.ApiEndpoints;
using DataReader.Services;
using Microsoft.AspNetCore.Mvc;

namespace DataReader.Endpoints.Insert;

public class InsertBodyValue
{
    [JsonPropertyName("value")] public string? Value { get; set; }
}

public class CreateQuery
{
    [FromRoute(Name = "id")] public string? Id { get; set; }
    [FromBody] public InsertBodyValue? BodyValue { get; set; }
}

public class InsertEndpoint : EndpointBaseAsync.WithRequest<CreateQuery>.WithActionResult
{
    private readonly ILogger<InsertEndpoint> _logger;
    private readonly IStorageConnector<string, string> _connector;

    public InsertEndpoint(ILogger<InsertEndpoint> logger, IStorageConnector<string, string> connector)
    {
        _logger = logger;
        _connector = connector;
    }

    [HttpPut("/api/create/{id}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(422)]
    [ProducesResponseType(409)]
    public override async Task<ActionResult> HandleAsync([FromRoute] CreateQuery request, CancellationToken cancellationToken = new ())
    {
        if (request.Id is null || request.BodyValue?.Value is null)
            return UnprocessableEntity();

        _logger.LogInformation("try to insert a new key {Key}", request.Id);
        var result = await _connector.InsertAsync(request.Id, request.BodyValue.Value);

        return (ActionResult)( result ? Ok() : Conflict());

    }
}
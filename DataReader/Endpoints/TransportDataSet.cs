﻿using System.Text.Json.Serialization;

namespace DataReader.Endpoints;

public class TransportDataSet
{
    [JsonPropertyName("id")] public string? Id { get; set; }
    [JsonPropertyName("value")] public string? Value { get; set; }
}
using Ardalis.ApiEndpoints;
using DataReader.Services;
using Microsoft.AspNetCore.Mvc;
using StackExchange.Redis;

namespace DataReader.Endpoints.Delete;

public class DeleteQuery
{
    [FromRoute(Name = "id")] public string? Id { get; set; }
}

public class DeleteEndpoint : EndpointBaseAsync.WithRequest<DeleteQuery>.WithActionResult
{
    private readonly IStorageConnector<string, string> _connector;
    private readonly ILogger<DeleteEndpoint> _logger;

    public DeleteEndpoint(ILogger<DeleteEndpoint> logger, IStorageConnector<string, string> connector)
    {
        _connector = connector;
        _logger = logger;
    }


    [HttpDelete("/api/delete/{id}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(422)]
    public override async Task<ActionResult> HandleAsync([FromRoute] DeleteQuery deleteQuery,
        CancellationToken cancellationToken = new())
    {
        _logger.LogInformation("try to delete cache entry with key {Key}", deleteQuery.Id);
        if (deleteQuery.Id is null)
            return UnprocessableEntity();

        return await _connector.DeleteAsync(deleteQuery.Id) ? Ok() : NotFound();


    }
}
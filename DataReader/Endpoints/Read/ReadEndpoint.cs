using Ardalis.ApiEndpoints;
using DataReader.Services;
using Microsoft.AspNetCore.Mvc;

namespace DataReader.Endpoints.Read;

public class ReadQuery
{
    [FromRoute(Name = "id")] public string? Id { get; set; }
}

public class ReadEndpoint : EndpointBaseAsync.WithRequest<ReadQuery>.WithActionResult<TransportDataSet>
{

    private readonly ILogger<ReadEndpoint> _logger;
    private readonly IStorageConnector<string, string> _connector;

    public ReadEndpoint(ILogger<ReadEndpoint> logger, IStorageConnector<string, string> connector)
    {
        _logger = logger;
        _connector = connector;
    }

    [HttpGet("/api/read/{id}")]
    [ProducesResponseType(200)]
    [ProducesResponseType(404)]
    [ProducesResponseType(422)]
    [Produces(typeof(TransportDataSet))]
    public override async Task<ActionResult<TransportDataSet>> HandleAsync([FromRoute] ReadQuery request,
        CancellationToken cancellationToken = new())
    {
        _logger.LogInformation("try to read a value for key {Key}", request.Id);
        
        if (request.Id is null)
            return UnprocessableEntity();
        
        var resultOpt = await _connector.ReadAsync(request.Id);
        
        return resultOpt.Item2.Match(
            Some: result => (ActionResult) Ok(new TransportDataSet {Id = resultOpt.Item1, Value = result}),
            None: () => (ActionResult) NotFound()
        );
    }
}
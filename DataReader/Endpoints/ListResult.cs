﻿using System.Text.Json.Serialization;

namespace DataReader.Endpoints;

public class ListResult
{
    [JsonPropertyName("results")] public IEnumerable<TransportDataSet> Results { get; set; } = new List<TransportDataSet>();
}
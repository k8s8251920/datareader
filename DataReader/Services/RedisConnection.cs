using StackExchange.Redis;

namespace DataReader.Services;


public static class RedisConnection
{
    public static void AddRedisConnection(this IServiceCollection serviceCollection, string connectionString)
    {
        var multiplexer = ConnectionMultiplexer.Connect(connectionString);
        serviceCollection.AddSingleton<IConnectionMultiplexer>(multiplexer);
    }
}
using LanguageExt;
using LanguageExt.SomeHelp;
using StackExchange.Redis;
using static LanguageExt.Prelude;

namespace DataReader.Services;

public interface IStorageConnector<TKey, TValue>
{
    Task<bool> DeleteAsync(TKey key);

    Task<bool> InsertAsync(TKey key, TValue value);

    Task<bool> UpdateAsync(TKey key, TValue value);

    Task<(TKey, Option<TValue>)> ReadAsync(TKey key);

    Task<IEnumerable<(TKey, TValue)>> ListAsync();
}

public class RedisStorageConnector : IStorageConnector<string, string>
{
    private readonly IConnectionMultiplexer _multiplexer;
    private readonly ILogger<RedisStorageConnector> _logger;

    public RedisStorageConnector(IConnectionMultiplexer multiplexer, ILogger<RedisStorageConnector> logger)
    {
        _multiplexer = multiplexer;
        _logger = logger;
    }

    public Task<bool> DeleteAsync(string key)
    {
        return Task.Run(() =>
        {
            if (key is null)
                throw new ArgumentNullException(nameof(key));

            _logger.LogInformation("delete a key {Key}", key);
            var database = _multiplexer.GetDatabase();
            if (!database.StringGet(key).HasValue)
            {
                _logger.LogWarning("cannot remove the key {Key}, it does not exists in database", key);
                return false;
            }

            database.KeyDelete(key.ToString());
            return true;
        });
    }

    public Task<bool> InsertAsync(string key, string value)
    {
        if (key is null)
            throw new ArgumentNullException(nameof(key));
        
        if (value is null)
            throw new ArgumentNullException(nameof(value));

        _logger.LogInformation("insert a value {Value} for key {Key}", value, key);
        var database = _multiplexer.GetDatabase();
        if (!database.StringGet(key).HasValue) return database.StringSetAsync(key, value);
        _logger.LogWarning("cannot insert key {Key}, because it exists in database, use update command instead", key);
        return Task.FromResult(false);
    }

    public Task<bool> UpdateAsync(string key, string value)
    {
        if (key is null)
            throw new ArgumentNullException(nameof(key));

        if (value is null)
            throw new ArgumentNullException(nameof(value));

        _logger.LogInformation("update a value {Value} for key {Key}", value, key);
        var database = _multiplexer.GetDatabase();
        if (database.StringGet(key).HasValue) return database.StringSetAsync(key, value);
        _logger.LogWarning(
            "cannot update key {Key}, because it does not exists in database, use insert command instead", key);
        return Task.FromResult(false);
    }

    public Task<(string, Option<string>)> ReadAsync(string key)
    {
        if (key is null)
            throw new ArgumentNullException(nameof(key));
        
        return Task.Run(() =>
        {
            _logger.LogInformation("read a value for key {Key}", key);
            var database = _multiplexer.GetDatabase();
            if (database.StringGet(key).HasValue)
                return ((string, Option<string>)) (key, database.StringGet(key).ToString());

            return (key, None);
        });
    }

    public Task<IEnumerable<(string, string)>> ListAsync()
    {
        return Task.Run(() =>
        {
            _logger.LogInformation("read all keys and values from redis");
            _logger.LogInformation("connect to the {Db}", _multiplexer.Configuration);
            var database = _multiplexer.GetDatabase();
            var endpoint = _multiplexer.GetEndPoints().First();
            return _multiplexer
                .GetServer(endpoint)
                .KeysAsync()
                .Select(redisKey => (redisKey.ToString(), database.StringGet(redisKey).ToString()))
                .ToEnumerable();
        });
    }
}